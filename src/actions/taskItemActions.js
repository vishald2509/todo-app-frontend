export function updateTask(task) {
  return {
    type: "UPDATE_TASK",
    payload: task
  };
}
