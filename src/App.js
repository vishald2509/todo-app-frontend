import React from 'react';
import {Switch, Route} from 'react-router-dom';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import WelcomePage from './components/WelcomePage';
import Default from './components/Default';
import SignUp from './components/SignUp';
import Navbar from './components/Navbar';
import SignIn from './components/SignIn';
import TaskList from './components/TaskList';
import ModalBox from './components/ModalBox';


function App() {
  return (
    <React.Fragment>
      <Navbar/>
      <Switch>
        <Route exact path="/" component={WelcomePage}/>
        <Route path="/signup" component={SignUp}/>
        <Route path="/signin" component={SignIn}/>
        <Route path="/tasklist" component={TaskList}/>
        <Route component={Default}/>
      </Switch>
      <ModalBox/>

    </React.Fragment>

  );
}

export default App;
