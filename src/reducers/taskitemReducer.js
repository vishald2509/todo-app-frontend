const taskitemReducer = (state = {
    task: [],
  },
  action) => {
  switch (action.type) {
    case "UPDATE_TASK":
      state = {
        ...state,
        task: [...state.task, action.payload]
      };
      break;

    default:
  }
  return state;
};

export default taskitemReducer;
