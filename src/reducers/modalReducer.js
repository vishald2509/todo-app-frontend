const modalReducer = (state = {
  modalOpen: true,
}, action) => {
  switch (action.type) {
    case "OPEN_MODAL":
      state = {
        ...state,
        modalOpen: true
      };
      break;
    case "CLOSE_MODAL":
      state = {
        ...state,
        modalOpen: false
      };
      break;

    default:
  }
  return state;
};

export default modalReducer;
