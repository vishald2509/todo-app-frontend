import styled from 'styled-components';

export const ButtonContainer = styled.button`
  text-transform: capitalize;
  font-size: 1.4rem;
  background: ${props => props.type? "var(--lightBlue)" : "transparent"};
  color: ${props => props.type? "var(--mainWhite)" : "var(--mainDark)"};
  border: 0.05rem solid var(--lightBlue);
  border-radius: 0.5rem;
  padding: 0.2rem 0.5rem;
  cursor: pointer;
  margin: 0.2rem 0.5rem 0.2rem 0;
  transition: all 0.5s ease-in-out;
  min-width: 10rem;
  &:hover{
    background: ${prop => prop.type ? "var(--mainBlue)" : "var(--lightBlue)"};
    color: var(--mainWhite);
    border-color: ${prop => prop.type ? "var(--mainBlue)" : "var(--lightBlue)"};
  }
  &:focus{
    outline: none;
  }
`;

export const InputContainer = styled.input`
  font-size: 1.4rem;
  background: transparent;
  border: 0.05rem solid var(--lightBlue);
  border-radius: 0.5rem;
  padding: 0.2rem 0.5rem;
  margin: 0.2rem 0.5rem 0.2rem 0;
  transition: all 0.5s ease-in-out;
  min-width: 10rem;
  &:focus{
    outline: none;
  }
`;


export const ItemContainer = styled.input`
  font-size: 1.4rem;
  background: transparent;
  border: 0;
  padding: 0;
  margin: 0;
  min-width: 10rem;
  max-width: 100rem;
  &:focus{
    outline: none;
  }
`;
//
//
// export const ItemCheck= styled.input`
//
//   background: transparent;
//   &:focus{
//     outline: none;
//   }
// `;
