import React, { Component } from 'react';
import { PageWrapper } from './PageWrapper';
import TaskList from './TaskList';


class AddTask extends Component {

  render() {
    return (
      <div className="container-fluid d-flex align-items-center flex-direction: column p-0 m-0" align="center">
          <PageWrapper className="mx-auto">
            <TaskList changemodal={this.props.changemodal}/>
          </PageWrapper>
    </div>
    );
  }

}


export default AddTask;
