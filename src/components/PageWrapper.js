import styled from 'styled-components';

export const PageWrapper = styled.div`

  background:  var(--mainWhite);
  margin-top: 80px;
  padding: 4rem;
  padding-bottom: 180px;
  box-shadow: 0 4px 8px 0 var(--mainDark), 0 6px 20px 0 var(--mainDark);
  min-width: 500px;

  .welcome-text{
    font-size: 30px;
    margin-bottom: 150px;
  }

  .btn-welcome{
    margin-bottom: 40px;
  }

  .header-text{
    font-size: 30px;
    margin-bottom: 10px;
  }
  .input{
    border: 0.05rem solid var(--lightBlue);
    border-radius: 0.5rem;
  }

`;
