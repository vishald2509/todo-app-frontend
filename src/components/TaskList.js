import React, { Component } from 'react';
import {ButtonContainer, ItemContainer } from './Button';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { updateTask } from '../actions/taskItemActions';

class TaskList extends Component {

  constructor(props){
      super(props);
      this.state ={
        taskitems: [{id:0, value:""}]
      };

    }

    onUpdate = (id, e) => {
      // finding state index of chaging input container
      const index = this.state.taskitems.findIndex((arr)=>{ return arr.id === id });
      const taskitem = Object.assign({}, this.state.taskitems[index]);
      //changing its value
      taskitem.value = e.target.value;
      //replacing by keeping history
      const {state} = this.state;
      const taskitems = Object.assign([], this.state.taskitems);
      taskitems[index] = taskitem;
      let flag = this.state.taskitems.findIndex((arr)=>{ return arr.id === id+1 });

      if(flag<0){
        this.setState(()=>{
          return {...state, taskitems: [...taskitems, {id: id+1, value: ""}]}
        }, ()=>console.log(`inside callback`, this.state)
      );
      }
      else{
        this.setState(()=>{
          return {taskitems};
        }, ()=>this.props.updateTask(this.state.taskitems)
        );
      }
      console.log(this.state);
    }

    removeItem = (index) => {
      let items = this.state.taskitems;
      items.splice(index, 1);
      this.setState({taskitems: items});
    }

  render() {
    return (
      <div>
      <div className="row">
        <div className="col-md-12 mx-auto">
          <div className="header-text text-center">Add New Task</div>
          <hr/><br/>
            <form>
              <div className="form-group scrollablediv" >
              { (this.state.taskitems.length > 0)?
                  <React.Fragment>

                  {
                    this.state.taskitems.map((item, index) =>
                    <React.Fragment key={item.id}>
                      <TaskContainer>
                        <div className="custom-control custom-checkbox ">
                          <input type="checkbox" className="custom-control-input"/>
                          <label className="custom-control-label">
                          <ItemContainer type="text" className="taskfield"  name={item.value} id={item.id} value={item.value} onChange={this.onUpdate.bind(this, item.id)} />

                            <i className="fa fa-times" aria-hidden="true"  onClick={this.removeItem.bind(this, index)}></i>
                          </label>
                        </div>
                      </TaskContainer>
                      <br/>
                    </React.Fragment>)
                  }

                  </React.Fragment>

                : ""  }

              </div>
              <ButtonContainer type="button" className="taskbutton">Submit</ButtonContainer>
              <ButtonContainer type="button" className="taskbutton" onClick={this.props.changemodal} >Close</ButtonContainer>
            </form>
          </div>
        </div>
      </div>
    );
  }

}


const TaskContainer = styled.div`

  display: inline-block !important;
  border-bottom: 2px solid var(--lightBlue);
  max-width: 100%;
  margin-bottom: 10px;
  .taskfield{
    border: 0;
    color: black;
    background: transparent;
    min-width: 200px;
    min-heigth: 200px !important;
    padding-bottom: 10px;
  }

`;


const mapStateToProps = (state) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateTask: (taskitems) => {
      dispatch(updateTask(taskitems));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
