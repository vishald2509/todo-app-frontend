import React, { Component } from 'react';
import { PageWrapper } from './PageWrapper';
import {ButtonContainer, InputContainer } from './Button';

class SignUp extends Component {

  constructor(){
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      phoneno: ""
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
  }

  onSubmit(e){
    e.preventDefault();
    const newUser ={
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
      phoneno: this.state.phoneno
    };
    console.log(newUser);
  }

  render() {
    return (
      <div className="container-fluid d-flex" align="center">
        <PageWrapper className="mx-auto">
              <div className="row">
                  <div className="col-md-8 mx-auto">
                      <div className="header-text text-center">SignUp</div>
                      <hr/><br/>
                      <form onSubmit={this.onSubmit}>
                          <div className="form-group">
                            <InputContainer type="text" className="form-control form-control-lg" name="username" placeholder="Enter Username" value={this.state.username} onChange={this.onChange}/>
                          </div>
                          <div className="form-group">
                            <InputContainer className="form-control form-control-lg" name="email" placeholder="Enter Email" value={this.state.email} onChange={this.onChange}/>
                          </div>
                          <div className="form-group">
                            <InputContainer className="form-control form-control-lg" name="password" placeholder="Enter password" value={this.state.password} onChange={this.onChange}/>
                          </div>
                          <div className="form-group">
                            <InputContainer className="form-control form-control-lg" name="phoneno" placeholder="Enter phoneno" value={this.state.phoneno} onChange={this.onChange}/>
                          </div>
                          <ButtonContainer type="submit">Submit</ButtonContainer>
                        </form>
                    </div>
                </div>
        </PageWrapper>
    </div>
    );
  }

}

export default SignUp;
