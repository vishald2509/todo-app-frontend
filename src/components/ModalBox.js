import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import AddTask from './AddTask';
import { openModal, closeModal } from '../actions/modalActions';

class ModalBox extends Component {

  constructor(){
    super();
    this.changemodal = this.changemodal.bind(this);
  }
  changemodal(){
    this.props.closeModal();
  }
  render() {

    if(this.props.modal.modalOpen){
      return(
        <ModalBoxContainer className="mx-auto">
          <AddTask changemodal={this.changemodal}/>
        </ModalBoxContainer>
      );
    }
    else {
      return null;
    }
  }
}

const ModalBoxContainer = styled.div`

  position: fixed;
  top:0;
  left:0;
  right: 0;
  bottom: 0;
  background: rgba(0,0,0,0.3);
  display:flex;
  align-items:center;
  justify-content: center;
`;



const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    openModal: () => {
      dispatch(openModal());
    },
    closeModal: () => {
      dispatch(closeModal());
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ModalBox);
