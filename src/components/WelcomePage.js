import React, { Component } from 'react';
import { ButtonContainer } from './Button';
import { Link } from 'react-router-dom';
import { PageWrapper } from './PageWrapper';

class WelcomePage extends Component {

  render() {
    return (


      <div className="container-fluid d-flex" align="center" >
        <PageWrapper className="mx-auto ">
        <div className="row">
          <div className="col-md-12 mx-auto welcome-text">Welcome To My ToDo Application</div>
        </div>

        <div className="row">
          <div className="col-md-12 mx-auto btn-welcome">
            <Link to="/signin">
              <ButtonContainer>SignIn</ButtonContainer>
            </Link>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mx-auto btn-welcome">
            <Link to="/signup">
              <ButtonContainer>SignUp</ButtonContainer>
            </Link>
          </div>
        </div>

        </PageWrapper>
      </div>
    );
  }

}



export default WelcomePage;
