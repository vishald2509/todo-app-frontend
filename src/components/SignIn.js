import React, { Component } from 'react';
import { PageWrapper } from './PageWrapper';
import {ButtonContainer, InputContainer } from './Button';

class SignIn extends Component {

  render() {
    return (
      <div className="container-fluid d-flex" align="center">
        <PageWrapper className="mx-auto">
              <div className="row">
                  <div className="col-md-8 mx-auto">
                      <div className="header-text text-center">SignIn</div>
                      <hr/><br/>
                      <form>
                          <div className="form-group">
                            <InputContainer type="text" className="form-control form-control-lg" name="username" placeholder="Enter Username" />
                          </div>
                          <div className="form-group">
                            <InputContainer className="form-control form-control-lg" name="password" placeholder="Enter password"/>
                          </div>
                          <ButtonContainer type="submit">Submit</ButtonContainer>
                        </form>
                    </div>
                </div>
        </PageWrapper>
    </div>
    );
  }

}

export default SignIn;
