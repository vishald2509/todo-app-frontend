import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

class Navbar extends Component {

  render() {
    return (
      <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5 bg-primary">
        <Link to="/" className="nav-link">ToDo App</Link>
      </NavWrapper>
    );
  }

}


const NavWrapper = styled.nav`
  background: var(--mainBlue);
  .nav-link{
    color:var(--mainWhite)!important;
    height: 4vh;
    min-height: 2vh;
    font-size:1.3rem;
    text-transform: capitalize;
    padding: 0;
    margin: 0;
  }

`;


export default Navbar;
