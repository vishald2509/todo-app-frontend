import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";

import modal from './reducers/modalReducer';
import taskitem from './reducers/taskitemReducer';

const initialState = { };


const store = createStore(
  combineReducers({modal, taskitem}),
  initialState,
  applyMiddleware(logger)
);

store.subscribe(()=>{
  console.log(store.getState());
});

export default store;
